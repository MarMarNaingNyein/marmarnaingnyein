﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _10Oct2019_Language
{
    class CountingWord_Sentence
    {
        static void Main(string[] args)
        {
            string paragraph = "NLP techniques are becoming widely popular scientific research areas as well as Information Technology industry." +
                " Language technology together with Information Technology can enhance the lives of people with different capabilities. " +
                "This system implements voice command mobile phone dialer application. The strength of the system is that it can make phone " +
                "call to the contact name written in either English scripts or Myanmar scripts.";
            string[] sentArr = paragraph.Split(".");
            string[] wordArr = paragraph.Split(" ");

            Console.WriteLine("Number of Setences : " + (sentArr.Length-1));
            Console.WriteLine("Number of words : " + wordArr.Length);
        }
    }
}
