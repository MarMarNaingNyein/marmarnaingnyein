﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _10Oct2019_Language
{
    class LaptopPrice
    {
        static void Main(string[] args)
        {
            string[] pcName = { "Lenovo", "HP", "Samsung", "Acer", "Dell", "Express" };
            string[] pcModel = { "core i3", "core i5", "core i7", "core i9" };
            double[,] pcPrice = new double[4, 6]{ { 230.21,400.21,2994.2,693.33,340.44,691.99},
                                                   {529.483,920.483,676.66,1594.659,783.012,1591.577},
                                                   {552.504,960.504,706.08,1663.992,817.056,1660.776 },
                                                   {690.63,1200.63,882.6,2079.99,1021.32,2075.97} };
            string inputName = "";
            string inputModel = "";

            string resultName = string.Empty;
            string resultModel = string.Empty;
            int nameIndex = 0;
            int modelIndex = 0;

            do
            {
                if (string.IsNullOrEmpty(resultName))
                {
                    Console.WriteLine("Choose Laptop Name: Lenovo, HP, Samsung, Acer, Dell, Express");
                    inputName = Console.ReadLine();

                    for (int j = 0; j < pcName.Length; j++)
                    {
                        if (pcName[j].ToLower().Equals(inputName.ToLower()))
                        {
                            resultName = inputName;
                            nameIndex = j;
                            break;
                        }

                    }
                }

                if (!string.IsNullOrEmpty(resultName) && string.IsNullOrEmpty(resultModel))
                {
                    Console.WriteLine("Choose Model Name: core i3, core i5, core i7, core i9");
                    inputModel = Console.ReadLine();

                    for (int j = 0; j < pcModel.Length; j++)
                    {
                        if (pcModel[j].ToLower().Equals(inputModel.ToLower()))
                        {
                            resultModel = inputModel;
                            modelIndex = j;
                            break;
                        }

                    }
                }

            } while (string.IsNullOrEmpty(resultName) || string.IsNullOrEmpty(resultModel));

            //Finding Result
            double resultPrice = pcPrice[modelIndex, nameIndex];
            Console.WriteLine("Enter Currency Exchange Rate : ");
            double exchangeRate = double.Parse(Console.ReadLine());
            Console.WriteLine("Price is " + (resultPrice * exchangeRate) + " MMK");
        }
    }
}
