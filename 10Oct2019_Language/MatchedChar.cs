﻿using System;

namespace _10Oct2019_Language
{
    class MatchedChar
    {
        static void Main(string[] args)
        {
            string st1;
            string st2;
            string matchChar = "";
            Boolean lenEqual = false;

            Console.WriteLine("Enter The First String:");
            st1 = Console.ReadLine();
            Console.WriteLine("Enter The Second String");
            st2 = Console.ReadLine();

            while (lenEqual == false)
            {
                if (st1.Length == st2.Length)
                {
                    char[] arr1 = st1.ToCharArray();
                    char[] arr2 = st2.ToCharArray();

                    lenEqual = true;
                    for (int i = 0; i < st1.Length; i++)
                    {
                        for (int j = 0; j < st2.Length; j++)
                        {
                            if (arr1[i] == arr2[j] && !(matchChar.Contains(arr1[i])))
                            {
                                matchChar += arr1[i];
                            }
                        }

                    }
                }
                else
                {
                    Console.WriteLine("Length of Two Strings are ");
                    Console.WriteLine("Enter The First String:");
                    st1 = Console.ReadLine();
                    Console.WriteLine("Enter The Second String");
                    st2 = Console.ReadLine();
                }
            }

            Console.WriteLine("Number of Common Character : " + matchChar.Length);
            char[] matchArr = matchChar.ToCharArray();
            Console.WriteLine("Matched Characters : ");
            for (int i = 0; i < matchArr.Length; i++)
            {
                Console.Write(matchArr[i] + " ");
            }


        }
    }
}
