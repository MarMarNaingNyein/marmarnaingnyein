﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _10Oct2019_Language
{
    class NRC_Information
    {
        static void Main(string[] args)
        {
            do
            {
                Console.WriteLine("Enter your NRC : ");
                string nrc = Console.ReadLine();
                string[] key = { "(N)", "(Naing)", "/" };
                string[] ans = nrc.Split(key, StringSplitOptions.RemoveEmptyEntries);
                Console.WriteLine("Your Township :" + ans[1]);
                Console.WriteLine("Your Number :" + ans[2]);
                Console.WriteLine("Do you want to exit (y/n)?");
            } while (Console.ReadLine() == "n");

        }
    }
}
