﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _10Oct2019_Language
{
    class SimplePresentQuestion
    {
        static void Main(string[] args)
        {
            string[] verb1 = { "am", "is", "are", "do", "does" };
            string sent = "";
            int found =0;
            Console.WriteLine("Enter a sentence");
            sent = Console.ReadLine();
            string[] wordArr = sent.Split(" ");
            if (sent.EndsWith("?"))
            {
                Console.WriteLine("This is a question sentence.");
                Console.WriteLine("The First Word is " + wordArr[0]);

                for (int i = 0; i < verb1.Length; i++)
                {
                    if (sent.Contains(verb1[i]) && !(sent.Contains("did")))
                    {
                        found = 1;
                        break;
                    }
                }
                if (found == 0)
                {
                    Console.WriteLine("The question is not simple present tense.");
                }
                else
                {
                    Console.WriteLine("The question is simple present tense.");
                }

            }
            else
            {
                Console.WriteLine("This is not question sentence.");
            }
        }
    }
}
