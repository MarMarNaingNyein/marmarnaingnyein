﻿using System;

namespace _11Oct2019_Language
{
    class ExceptionAssignment
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a series of number");
            string[] input = Console.ReadLine().Split(" ");
            Result result = new Result(input);

        }
    }

    class Result
    {
        public Result(string[] input)
        {
            int[] num = new int[input.Length];
            int sum = 0;
            try
            {
                for (int i = 0; i < input.Length; i++)
                {
                    num[i] = Convert.ToInt32(input[i]);
                    sum += num[i];

                }
                Array.Sort(num);
                Console.WriteLine("Average : " + (sum / num.Length));
                Console.WriteLine("Minimun : " + num[0]);
                Console.WriteLine("Maximum : " + num[num.Length - 1]);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}
