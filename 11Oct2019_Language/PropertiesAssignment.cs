﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _11Oct2019_Language
{
    class PropertiesAssignment
    {
        public static Student[] stdArr = new Student[4];
        public static int[] mark = new int[4];
        public static int sum = 0;
        static void Main(string[] args)
        {
            InputAllData();
            Student std = new Student();
            std.Display(stdArr);

            Console.WriteLine("Enter Student ID you want to know about.");
            int inputId = Convert.ToInt32(Console.ReadLine());
            FindStudentById(inputId);
            Console.WriteLine();
            Array.Sort(mark);
            Console.WriteLine("Average Mark is " + sum / stdArr.Length);
            Console.WriteLine();
            BestStudent();
        }

        public static void InputAllData()
        {
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine("Enter student Data " + (i + 1) + " : Id Name Mark ");
                string[] input = Console.ReadLine().Split(" ");
                stdArr[i] = new Student(Convert.ToInt32(input[0]), input[1], Convert.ToInt32(input[2]));
                mark[i] = stdArr[i].studentMark;
                sum += mark[i];
            }
        }

        public static void FindStudentById(int inputId)
        {
            int found = 0;
            for (int j = 0; j < stdArr.Length; j++)
            {
                if (stdArr[j].studentId == inputId)
                {
                    Console.WriteLine("Student Id : " + stdArr[j].studentId + ", Student Name : " + stdArr[j].studentName +
                        ", Student Mark : " + stdArr[j].studentMark);
                    found = 1;
                    break;
                }
            }
            if (found == 0)
            {
                Console.WriteLine("Student Not Found!");
            }
        }

        public static void BestStudent()
        {
            Console.WriteLine("Student who got the highest mark is :");
            foreach (Student s in stdArr)
            {
                if (s.studentMark == mark[mark.Length - 1])
                {
                    Console.WriteLine("Student Id : " + s.studentId + ", Student Name : " + s.studentName + ", Student Mark : " + s.studentMark);
                    break;
                }
            }
        }
    }
    class Student
    {
        public int studentId;
        public string studentName;
        public int studentMark;

        public int StuId
        {
            get { return studentId; }
            set { studentId = value; }
        }
        public string StuName
        {
            get { return studentName; }
            set { studentName = value; }
        }
        public int StuMark
        {
            get { return studentMark; }
            set { studentMark = value; }
        }

        public Student()
        {

        }

        public Student(int id, string name, int mark)
        {
            this.StuId = id;
            this.StuName = name;
            this.StuMark = mark;
        }

        public void Display(Student[] stdArr)
        {
            for (int i = 0; i < stdArr.Length; i++)
            {
                Console.Write("StudentId : " + stdArr[i].studentId + ", StudentName: "
                    + stdArr[i].studentName + " , StudentMark : " + stdArr[i].studentMark);
                Console.WriteLine();
            }
        }

    }
}
