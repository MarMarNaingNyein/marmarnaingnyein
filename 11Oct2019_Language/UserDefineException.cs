﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _11Oct2019_Language
{
    class UserDefineException
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Enter Mark :");
                int mark = Convert.ToInt32(Console.ReadLine());
                if (mark < 0 || mark > 100)
                {
                    throw new InvalidMarkException(mark.ToString());
                }
            }
            catch (InvalidMarkException)
            {
                Console.WriteLine("Error Occur");
            }
            catch (Exception)
            {
                Console.WriteLine("System error occur.");
            }
        }
    }
    class InvalidMarkException : Exception
    {
        public InvalidMarkException(String me)
        {
            Console.WriteLine(me + " is Invalid Mark.");
        }
    }
}
