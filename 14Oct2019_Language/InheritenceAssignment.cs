﻿using System;

namespace _14Oct2019_Language
{
    class InheritenceAssignment
    {
        static void Main(string[] args)
        {
            B.SM();
            C.SM();

            C c = new C();
            B b = new B();
            b = c;

            b.VIM();
            b.NIM();


            c.VIM();
            c.NIM();

        }
    }
    class B
    {
        public static void SM() { Console.WriteLine("Hello from B.SM()"); }
        public virtual void VIM() { Console.WriteLine("Hello from B.VIM()"); }
        public void NIM() { Console.WriteLine("Hello from B.NIM()"); }
    }

    class C : B
    {
        new public static void SM()
        {
            Console.WriteLine("Hello from C.SM() hiding B");
        }

        public override void VIM() { Console.WriteLine("Hello from C.VIM() hiding B"); }

        new public void NIM() { Console.WriteLine("Hello from C.NIM() hiding B"); }
    }
}
