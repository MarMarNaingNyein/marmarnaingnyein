﻿using System;
using System.Collections.Generic;

namespace _16Oct_Language
{
    class SortedDicAssigment
    {
        static SortedDictionary<int, Student1> sortedDic = new SortedDictionary<int, Student1>();
        static void Main(string[] args)
        {
            do
            {
                InsertNewStudent();
                Console.WriteLine("Do you want to insert more student (y/n)?");
            } while (Console.ReadLine().Equals("y"));

            DisplayAllData();

            Console.Write("Enter a student Roll No you want to search : ");
            int rollNo = Convert.ToInt32(Console.ReadLine());
            SearchByRollNo(rollNo);

            Console.Write("Enter a student Roll No you want to delete : ");
            rollNo = Convert.ToInt32(Console.ReadLine());
            bool success = DeleteByRollNo(rollNo);
            if (success == false)
            {
                Console.WriteLine("Student Not Found!");
            }
            else
            {
                Console.WriteLine("After Deleting");
                DisplayAllData();
            }
        }
        static void InsertNewStudent()
        {
            Console.Write("Enter Student roll no: ");
            int rollno = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter Student name: ");
            string name = Console.ReadLine();
            Console.Write("Enter Student address: ");
            string address = Console.ReadLine();
            Student1 student = new Student1(rollno, name, address);
            sortedDic.Add(student.RollNo, student);
        }
        static void DisplayAllData()
        {
            foreach (KeyValuePair<int, Student1> kvp in sortedDic)
            {
                Student1 stdObj = kvp.Value;
                Console.WriteLine("Student Id: " + stdObj.RollNo + " , Student Name: " +
                    stdObj.Name + " , Student Address: " + stdObj.Address);
            }
        }
        static void SearchByRollNo(int rno)
        {
            bool found = false;
            foreach (KeyValuePair<int, Student1> kvp in sortedDic)
            {
                Student1 stdObj = kvp.Value;
                if (stdObj.RollNo == rno)
                {
                    Console.WriteLine("Student Id: " + stdObj.RollNo + " , Student Name: " +
                        stdObj.Name + " , Student Address: " + stdObj.Address);
                    found = true;
                    break;
                }
            }
            if (found == false)
            {
                Console.WriteLine("Student Not Found!");
            }
        }

        static bool DeleteByRollNo(int rno)
        {
            bool found = false;
            foreach (KeyValuePair<int, Student1> kvp in sortedDic)
            {
                Student1 stdObj = kvp.Value;
                if (kvp.Key == rno)
                {
                    found = true;
                    sortedDic.Remove(rno);
                    break;
                }
            }
            return found;
        }
    }
    
    }
class Student1
{
    int rollNo;
    string name;
    string address;
    public int RollNo
    {
        get { return rollNo; }
        set { rollNo = value; }
    }
    public string Name
    {
        get { return name; }
        set { name = value; }
    }
    public string Address
    {
        get { return address; }
        set { address = value; }
    }
    public Student1(int sId, string sName, string sAddress)
    {
        rollNo = sId;
        name = sName;
        address = sAddress;
    }
}
