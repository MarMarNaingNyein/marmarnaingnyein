﻿using System;
using System.Collections.Generic;
using System.Text;


namespace _16Oct_Language
{
    public class Student
    {
            public int rollNo;
            public string name;
            public string address;
            public int RollNo
            {
                get { return rollNo; }
                set { rollNo = value; }
            }
            public string Name
            {
                get { return name; }
                set { name = value; }
            }
            public string Address
            {
                get { return address; }
                set { address = value; }
            }
            public Student(int sId, string sName, string sAddress)
            {
                rollNo = sId;
                name = sName;
                address = sAddress;
            }
        }
    }
