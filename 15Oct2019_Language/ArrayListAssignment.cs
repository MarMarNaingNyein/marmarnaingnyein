﻿using System;
using System.Collections;

namespace _15Oct2019_Language
{
    class ArrayListAssignment
    {
        static void Main(string[] args)
        {
            ArrayList studentNameArr = new ArrayList();
            studentNameArr.Add("Kaung Kaung");
            studentNameArr.Add("Mg Mg");
            studentNameArr.Add("Zin Bo");
            studentNameArr.Add("Htet Aung");
            studentNameArr.Add("Aung Aung");
            studentNameArr.Add("Hnin Si");
            studentNameArr.Add("Khin Yadanar");
            
            //display data
            Console.WriteLine("Before sorting: ");
            DisplayAllData(studentNameArr);

            studentNameArr.Sort();
            Console.WriteLine("After sorting: ");
            DisplayAllData(studentNameArr);

            Console.WriteLine("Enter a name from list that you want to search :");
            string stdName = Console.ReadLine();
            SearchByName(studentNameArr , stdName);

            Console.WriteLine("the students whose name starts with 'K' or 'k'");
            SearchByK(studentNameArr);

            RemoveByG(studentNameArr);
            Console.WriteLine("After Removing the name ending with 'G' or 'g' : ");
            DisplayAllData(studentNameArr);

            studentNameArr.Clear();  
        }
        public static void DisplayAllData(ArrayList studentNameArr)
        {
            for (int i = 0; i < studentNameArr.Count; i++)
            {
                Console.WriteLine(studentNameArr[i]);
            }
        }
        public static void SearchByName(ArrayList studentNameArr, string stdName)
        {
            if (studentNameArr.Contains(stdName))
            {
                Console.WriteLine("The position of " + stdName + " is :" + studentNameArr.IndexOf(stdName));
            }
            else
            {
                Console.WriteLine("This name does not exit in the list. So it is inserted!");
                studentNameArr.Add(stdName);
                DisplayAllData(studentNameArr);
            }
        }

        public static void SearchByK(ArrayList studentNameArr)
        {
            for (int i = 0; i < studentNameArr.Count; i++)
            {
                if (studentNameArr[i].ToString().ToLower().StartsWith("k"))
                {
                    Console.WriteLine(studentNameArr[i]);
                }
            }
        }

        public static void RemoveByG(ArrayList studentNameArr)
        {
            for(int i = 0; i < studentNameArr.Count; i++)
            {
                if (studentNameArr[i].ToString().ToLower().EndsWith("g"))
                 {
                   studentNameArr.Remove(studentNameArr[i]);
                   i = 0;
                 }
            }
        }
    }
}
