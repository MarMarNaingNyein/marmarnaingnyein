﻿using System;

namespace _15Oct2019_Language
{
    class PolymorphismTest
    {
        static void Main(string[] args)
        {
            IShape[] shapes = { new Cube(), new Circle() };

            Console.WriteLine("Cube Area : " + shapes[0].area());
            Console.WriteLine("Cube Volumn : " + shapes[0].volumn());

            Console.WriteLine("Circle Area : " + shapes[1].area());
            Console.WriteLine("Circle Volumn : " + shapes[1].volumn());
        }

    }

    public interface IShape
    {
        double area();
        double volumn();
    }


    class Cube : IShape
    {
        int x = 10;
        public double area()
        {
            return 6 * x * x;
        }

        public double volumn()
        {
            return x * x * x;
        }
    }

    class Circle : IShape
    {
        int radius = 10;
        public double area()
        {
            return (3.142 * radius * radius);
        }

        public double volumn()
        {
            return 0;
        }
    }
}
