﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace _15Oct2019_Language
{
    class QueueAssignment
    {
        static void Main(string[] args)
        {
            Queue studentNameQ = new Queue();
            studentNameQ.Enqueue("Kaung Kaung");
            studentNameQ.Enqueue("Mg Mg");
            studentNameQ.Enqueue("Zin Bo");
            studentNameQ.Enqueue("Htet Aung");
            studentNameQ.Enqueue("Hnin Si");
            studentNameQ.Enqueue("Khin Yadanar");

            Console.WriteLine("Before Sorting:");
            DisplayAllData(studentNameQ);

            ArrayList studentArr = new ArrayList(studentNameQ);
            studentArr.Sort();
            studentNameQ = new Queue(studentArr);
            Console.WriteLine("After Sorting:");
            DisplayAllData(studentNameQ);

            Console.WriteLine("Enter a name form List : ");
            string stdName = Console.ReadLine();
            bool found = SearchStudentByName(studentNameQ, stdName);
            if (found == true)
            {
                Console.WriteLine("The position of " + stdName + " : " + studentArr.IndexOf(stdName));
            }
            else
            {
                Console.WriteLine("Student Name Not Found!");
            }

            Console.WriteLine("Enter new student name");
            string newName = Console.ReadLine();
            InsertNewName(studentNameQ, newName);
            SearchByK(studentNameQ);

            studentArr = new ArrayList(studentNameQ);
            RemoveByG(studentArr);
            Console.WriteLine("After removing the name ending with 'G'");
            studentNameQ = new Queue(studentArr);
            DisplayAllData(studentNameQ);
            studentNameQ.Clear();
        }
        public static void DisplayAllData(Queue studentNameQ)
        {
            int num = 1;
            foreach (string name in studentNameQ)
            {
                Console.WriteLine(num + ". " + name);
                num++;
            }
        }

        public static Boolean SearchStudentByName(Queue studentNameQ, string stdName)
        {
            bool found = false;
            if (studentNameQ.Contains(stdName))
            {
                found = true;
            }
            return found;
        }

        public static void InsertNewName(Queue studentNameQ, string newName)
        {
            Boolean found = SearchStudentByName(studentNameQ, newName);
            if (found == true)
            {
                Console.WriteLine("Student name already exist!");
            }
            else
            {
                studentNameQ.Enqueue(newName);
                Console.WriteLine("After Inserting!");
                DisplayAllData(studentNameQ);
            }
        }

        public static void SearchByK(Queue studentNameQ)
        {
            Console.WriteLine("Student names starting with 'k' are");
            int num = 1;
            foreach (string name in studentNameQ)
            {
                if (name.ToString().ToLower().StartsWith("k"))
                {
                    Console.WriteLine((num) + ". " + name);
                }
                num++;
            }
        }

        public static void RemoveByG(ArrayList studentArr)
        {
            for (int i = 0; i < studentArr.Count; i++)
            {
                if (studentArr[i].ToString().ToLower().EndsWith("g"))
                {
                    studentArr.Remove(studentArr[i]);
                    i = 0;
                }
            }
        }
    }
}