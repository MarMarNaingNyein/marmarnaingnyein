﻿using System;
using System.Collections;
using System.Text;

namespace _15Oct2019_Language
{
    class StackAssignment
    {
          static void Main(string[] args)
            {
                Stack stdNameStack = new Stack();
                stdNameStack.Push("Kaung Kaung");
                stdNameStack.Push("Mg Mg");
                stdNameStack.Push("Zin Bo");
                stdNameStack.Push("Htet Aung");
                stdNameStack.Push("Hnin Si");
                stdNameStack.Push("Khin Yadanar");

                Console.WriteLine("Before Sorting:");
                DisplayAllData(stdNameStack);

                ArrayList studentArr = new ArrayList(stdNameStack);
                studentArr.Sort();
                studentArr.Reverse();
                stdNameStack = new Stack(studentArr);
                Console.WriteLine("After Sorting:");
                DisplayAllData(stdNameStack);

                Console.WriteLine("Enter a name form List : ");
                string stdName = Console.ReadLine();
                bool found = SearchStudentByName(stdNameStack, stdName);
                if (found == true)
                {
                    Console.WriteLine("The position of " + stdName + " : " + studentArr.IndexOf(stdName));
                }
                else
                {
                    Console.WriteLine("Student Name Not Found!");
                }

                Console.WriteLine("Enter new student name");
                string newName = Console.ReadLine();
                InsertNewName(stdNameStack, newName);
                SearchByK(stdNameStack);

                studentArr = new ArrayList(stdNameStack);
                RemoveByG(studentArr);
                Console.WriteLine("After removing the name ending with 'G'");
                stdNameStack = new Stack(studentArr);
                DisplayAllData(stdNameStack);
                stdNameStack.Clear();
            }
            public static void DisplayAllData(Stack stdNameStack)
            {
                int num = 1;
                foreach (string name in stdNameStack)
                {
                    Console.WriteLine(num + ". " + name);
                    num++;
                }
            }

            public static Boolean SearchStudentByName(Stack stdNameStack, string stdName)
            {
                bool found = false;
                if (stdNameStack.Contains(stdName))
                {
                    found = true;
                }
                return found;
            }

            public static void InsertNewName(Stack studentNameQ, string newName)
            {
                Boolean found = SearchStudentByName(studentNameQ, newName);
                if (found == true)
                {
                    Console.WriteLine("Student name already exist!");
                }
                else
                {
                    studentNameQ.Push(newName);
                    Console.WriteLine("After Inserting!");
                    DisplayAllData(studentNameQ);
                }
            }

            public static void SearchByK(Stack stdNameStack)
            {
                Console.WriteLine("Student names starting with 'k' are");
                int num = 1;
                foreach (string name in stdNameStack)
                {
                    if (name.ToString().ToLower().StartsWith("k"))
                    {
                        Console.WriteLine((num) + ". " + name);
                    }
                    num++;
                }
            }

            public static void RemoveByG(ArrayList studentArr)
            {
                for (int i = 0; i < studentArr.Count; i++)
                {
                    if (studentArr[i].ToString().ToLower().EndsWith("g"))
                    {
                        studentArr.Remove(studentArr[i]);
                        i = 0;
                    }
                }
            }

        }
}
