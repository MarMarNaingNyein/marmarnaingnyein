--creating HR database
CREATE DATABASE HR_DB;

--creating department table
CREATE TABLE department
(
deptNo INT NOT NULL PRIMARY KEY,
deptName VARCHAR(50) NOT NULL
);

--creating employee
CREATE TABLE employee
(
empNo INT NOT NULL PRIMARY KEY,
empName VARCHAR(30) NOT NULL,
salary INT DEFAULT 2000,
deptNo INT,
jobPosition VARCHAR(100),
registerDate Date,
address VARCHAR(30),
dob Date,
phoneNo VARCHAR(20),
FOREIGN KEY (deptNo) REFERENCES department (deptNo)
);

