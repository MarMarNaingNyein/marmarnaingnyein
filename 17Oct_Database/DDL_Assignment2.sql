--creating SALEPEOPLE table
CREATE TABLE SALESPEOPLE
(
Snum INT NOT NULL PRIMARY KEY,
Sname VARCHAR(50),
City VARCHAR(50),
Comm DECIMAL(3,2)
);

--creating CUSTOMERS table
CREATE TABLE CUSTOMERS
(
Cnum INT NOT NULL PRIMARY KEY,
Cname VARCHAR(50),
City VARCHAR(50),
Rating INT,
Snum INT,
FOREIGN KEY (Snum) REFERENCES SALESPEOPLE(Snum),
);

--creating ORDERS table
CREATE TABLE ORDERS
(
Onum INT NOT NULL PRIMARY KEY,
Amt DECIMAL(7,2),
Odate DATE,
Cnum INT,
Snum INT,
FOREIGN KEY (Snum) REFERENCES SALESPEOPLE(Snum),
FOREIGN KEY (Cnum) REFERENCES CUSTOMERS(Cnum)
);

--adding a column "DateOfBirth" with Date type in SALESPEOPLE
ALTER TABLE SALESPEOPLE
ADD DateOfBirth DATE;

--changing the data type of the column named ?DateOfBirth? (date) to datatype (datetime) in the ?SALESPEOPLE? table.
ALTER TABLE SALESPEOPLE
ALTER COLUMN DateOfBirth DATETIME;

--deleteing the column named "DateOfBirth" in the "SALESPEOPLE" table
ALTER TABLE SALESPEOPLE
DROP COLUMN DateOfBirth;