use HR_DB;

insert into department values (4,'Logistic');

insert into employee (empNo,empName,salary,deptNo,registerDate)
values (111,'Kyaw Kyaw',50000,4,'2019-02-2');

update employee set address = 'Magway', dob = '1998-3-6' , 
jobPosition = 'Accountant' , phoneNo = 09266453312 where empNo = 111;

select * from employee,department where employee.empNo = 111 and
employee.deptNo=department.deptNo;

update employee set salary = salary + (salary * 0.05);

select * from employee;

select * from employee,department where 
(employee.empNo = 109 or employee.empNo = 110) and
employee.deptNo = department.deptNo;

delete from employee where (employee.empNo = 109 or employee.empNo = 110);
 