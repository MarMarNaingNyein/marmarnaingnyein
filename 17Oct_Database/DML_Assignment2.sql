use HR_DB;

select Max(salary)'Maximum Salary' from employee;

select MIN(salary)'Minium Salary' from employee;

select COUNT(empNo) 'Number of Employee' from employee where registerDate >= '2013-02-12';

select address from employee group by address;

select SUM(salary) from employee;

select MAX(salary)'Highest Salary',address from employee group by address; 

--to retrieve minium salary ,maximum salary , average salary in each department
select min(salary)'Minium Salary',max(salary)'Maximun Salary',AVG(salary)'Average Salary',deptName
from employee,department where employee.deptNo=department.deptNo group by deptName;

--to retrieve the date of birth of oldest employee in each department
select deptName'deptName', min(registerDate)'Date Of Birth of Oldest Employee' from employee,department where
 employee.deptNo=department.deptNo group by deptName;

--to count the number of employee who have the same salary amount
select count(empNo)'Number of Employee',salary from employee group by salary;

--Average salary in department 4
select AVG(salary)'Average Salary' from employee where deptNo=4;

--departments who have a salary bill greater than 11000
SELECT deptName,salary FROM employee,department WHERE employee.deptNo=department.deptNo AND Salary>11000;

--departments who have a salary bill greater than 11000 using Having Clause
SELECT deptName,Salary FROM employee, department WHERE employee.deptNo = department.deptNo
GROUP BY Salary,deptName HAVING employee.Salary > 11000;




