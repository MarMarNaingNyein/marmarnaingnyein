--the order number, amount, and date for all rows in the Orders table.
select Onum'Order Number',Amt'Amount',Odate'Order Date' from ORDERS;

--customer information of salesperson�s number 1001
select * from CUSTOMERS where Snum = 1001;


select City,Sname,Snum,Comm from SALESPEOPLE;

select Snum,Onum from ORDERS 
where Onum in (Select Snum from ORDERS group by Snum);

--salespeople in London with a commission above .10.
select Sname,City,Comm from SALESPEOPLE 
where City = 'London' and Comm > 0.10;

--exclude all customers with a rating <= 100.
select * from CUSTOMERS where Rating > 100;

--customer names begin with the letter �C�.
select * from CUSTOMERS where Cname Like 'C%';

--orders except those with zeroes or NULLs in the amt field.
select * from ORDERS where Amt != 0 OR Amt != null;

--customer names with snum 1001 ,1002 and 1007
select Cname,Snum from CUSTOMERS where Snum = 1001 or Snum = 1002 or Snum = 1007;

--salesperson details not having commission .10,.13 or .15.
select * from SALESPEOPLE where Comm != 0.10 and Comm != 0.13 and Comm != 0.15;

--different city names in the CUSTOMER table.
select city from CUSTOMERS group by City;

