use HR_DB;

--start date of the employee who was the last to join
create procedure procedure_LastEmp
As
Begin
select max(registerDate)'Start Date' from employee
end;

--start date of the employee who has been with the company the longest.
select min(registerDate)'Start Date' from employee;

--employees have been on the same department
select COUNT(empNo)'Number Of Employee',department.deptName
from employee,department
where employee.deptNo = department.deptNo
group by employee.deptNo,department.deptName;
 
