﻿using EF_Assignment1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_Assignment
{
    class E_FAssignment1
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1.Add New Employee");
            Console.WriteLine("2.Modify Employee data");
            Console.WriteLine("3.Information Retrieval");

        }

        static void AddNewData()
        {
            Console.WriteLine("Enter Employee Id");
            int empId = Convert.ToInt32(Console.ReadLine());
            using (var db = new HR_DBEntities())
            {
                employee newEmp = new employee();
                newEmp.address = "Yangon";
                newEmp.deptNo = 2;
                newEmp.dob = DateTime.Now;
                newEmp.empName = "Kyaw Kyaw";
                newEmp.salary = 50000;
                newEmp.empNo = 200;
                newEmp.jobPosition = "Programmer";
                db.employees.Add(newEmp);
                db.SaveChanges();
                Console.WriteLine("Insert Success!");
            }

        }
    }
}
