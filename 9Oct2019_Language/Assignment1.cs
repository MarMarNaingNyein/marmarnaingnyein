﻿using System;

namespace _9Oct2019_Language
{
    class Assignment1
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Your Name:");
            string name = Console.ReadLine();
            Console.WriteLine("Enter Your roll number:");
            int rollno = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Your University Name:");
            string uni_name = Console.ReadLine();
            Console.WriteLine("Enter Your Date of Birth (MM/DD/YY):");
            DateTime dob = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine("Enter Your address:");
            string address = Console.ReadLine();

            Console.WriteLine("Your Personal Information :");
            Console.WriteLine("Name :" + name);
            Console.WriteLine("Roll No: " + rollno);
            Console.WriteLine("University Name :" + uni_name);
            Console.WriteLine("Date of Birth :" + dob.Day +"/" + dob.Month +"/" +dob.Year);
            Console.WriteLine("Address :" + address);
        }
    }
}
