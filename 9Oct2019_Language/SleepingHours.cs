﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _9Oct2019_Language
{
    class SleepingHours
    {
        static void Main(string[] args)
        {
            TimeSpan goTime;
            TimeSpan upTime;
            TimeSpan sleepHour;
            string[] bedTime = new string[2];
            string[] getUpTime = new string[2];
            TimeSpan hour24 = TimeSpan.Parse("1:00:00:00");
            do
            {

                Console.WriteLine("When do You go bed? (eg. 10:00 PM)");


                bedTime = Console.ReadLine().Split(" ");
                goTime = TimeSpan.Parse(bedTime[0]);
               
                // Console.WriteLine(goTime);
                Console.WriteLine("When do You get up? (eg. 8:00 AM)");
                getUpTime = Console.ReadLine().Split(" ");
                upTime = TimeSpan.Parse(getUpTime[0]);

                // Console.WriteLine(upTime);
                if (bedTime[1] == getUpTime[1])
                {
                    if (bedTime[0] == "12")
                    {
                        sleepHour = upTime;
                    }
                    else if (TimeSpan.Parse(bedTime[0]) > TimeSpan.Parse(getUpTime[0]))
                    {
                        sleepHour = hour24.Subtract(goTime).Add(upTime);
                    }
                    else
                    {
                        sleepHour = upTime.Subtract(goTime);
                    }
                }

                else
                {

                    sleepHour = (TimeSpan.Parse("12:00").Subtract(goTime)).Add(upTime);
                }


                Console.WriteLine("Sleep Hour is " + sleepHour);
                if (sleepHour == TimeSpan.Parse("8:00"))
                {
                    Console.WriteLine("You take care your health well!");
                }
                else if (sleepHour < TimeSpan.Parse("5:00"))
                {
                    Console.WriteLine("You are very hardworking!");
                }
                else if (sleepHour > TimeSpan.Parse("8:00"))
                {
                    Console.WriteLine("You are abnormal!");
                }
                Console.WriteLine("Enter (exit) if you want to quit!");
                
            } while ( Console.ReadLine() != "exit");
        }
    }
}
